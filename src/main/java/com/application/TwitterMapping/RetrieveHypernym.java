package com.application.TwitterMapping;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.rdf4j.model.Model;

public class RetrieveHypernym {


	
	public static String retrieve(String u) {
		URL url;
	    InputStream is = null;
	    BufferedReader br;
	    String line, result="";
	    System.out.println("URL!!!!!"+u);
	   String hypernym="null";
	    
	        try {
				url = new URL("http://wordnet-rdf.princeton.edu/ttl/id/"+u);
			
	        HttpURLConnection conn =(HttpURLConnection) url.openConnection();
	        int status = conn.getResponseCode();
	       // System.out.println(status);
	        
	        if(status!=500) {
		        is = url.openStream(); 
		        br = new BufferedReader(new InputStreamReader(is));
		        
		        while ((line = br.readLine()) != null) {
		        	//System.out.println("!!!!!!!!!!!!"+line);
		        	if(line.contains("wn:hypernym")) {
		        		hypernym=line.replaceAll("wn:hypernym pwnid:", "");
		        		int end=hypernym.length();
		        		if(hypernym.contains(";")) {
		        			end=hypernym.indexOf(";");
		        		}
		        		else if(hypernym.contains(".")) {
		        			end=hypernym.indexOf(".");
		        		}
		        		hypernym=hypernym.substring(0, end);
		        		
		        		hypernym=hypernym.replaceAll(" ", "");
		        		hypernym=hypernym.replaceAll("wn:causespwnid:", "");
		        		hypernym=hypernym.replaceAll("wn:similarpwnid:", "");
		        		System.out.println("hypernym:"+hypernym);
		        	}
		        }
	        }
	        } catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        return hypernym;
	}
	    
	
	
	
}
