package com.application.TwitterMapping;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import java.io.PrintWriter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.vocabulary.OWL;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.model.vocabulary.RDFS;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.http.HTTPRepository;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;

public class Mapping {
	
	/*public static Gson gson;
	public static JsonElement json;
	public static JsonObject jobject;
	public static ValueFactory factory;
	public static JsonArray jarray;
	public static String imagecount;
	public static String maskcount;
	public static ArrayList<Location> loc;*/

	public static Model buildModel(String jsonLine) throws JsonSyntaxException, JsonIOException, IOException {
		
		boolean istop;
		String uuid = UUID.randomUUID().toString().replaceAll("-", "");
		int t=0;
		int l=0;
		int labels_sum=1;
		int tweets_sum=1;
		ModelBuilder modelbuilder = new ModelBuilder();
		// Write json input in a file and then read json file
		PrintWriter writer = new PrintWriter("json.json", "UTF-8");
    	writer.println(jsonLine);
    	writer.close();
    	Gson gson = new Gson();
    	JsonElement json = gson.fromJson(new FileReader("json.json"), JsonElement.class);
    	JsonArray jarray = json.getAsJsonArray();
    	ValueFactory factory = SimpleValueFactory.getInstance();
		
		if(jarray.size()!=0) {
    	   	
    			modelbuilder.setNamespace("as", Namespaces.AS).setNamespace("oa", Namespaces.OA).setNamespace("owl",Namespaces.OWL).setNamespace("rdf", Namespaces.RDF).
				setNamespace("rdfs", Namespaces.RDFS).setNamespace("ta", Namespaces.TA).setNamespace("tweets", Namespaces.TWEETS).setNamespace("xsd", Namespaces.XSD)
				.setNamespace("wgs84_pos", Namespaces.WGS84);
    		/*modelbuilder.add(Namespaces.TA, RDF.TYPE, OWL.ONTOLOGY);
    		
    			modelbuilder.add(Namespaces.TA, OWL.IMPORTS, factory.createIRI(Namespaces.OA.replace("#", "")));
    			modelbuilder.add(Namespaces.TA, OWL.IMPORTS, factory.createIRI("https://www.w3.org/ns/activitystreams-owl"));
    			modelbuilder.add(Namespaces.TA, OWL.VERSIONINFO, "Created with TopBraid Composer");
    			
    			modelbuilder.add("ta:ClusterBody", RDF.TYPE, OWL.CLASS);
    			modelbuilder.add("ta:ClusterBody", RDFS.SUBCLASSOF, "as:Object");
    			
    			modelbuilder.add("ta:Label", RDF.TYPE, OWL.CLASS);
    			modelbuilder.add("ta:Label", RDFS.SUBCLASSOF, "as:Object");
    			
    			modelbuilder.add("ta:Tweet", RDF.TYPE, OWL.CLASS);
    			modelbuilder.add("ta:Tweet", RDFS.SUBCLASSOF, "as:Note");
    			
    			modelbuilder.add("ta:labels", RDF.TYPE, OWL.OBJECTPROPERTY);
    			modelbuilder.add("ta:labels", RDFS.DOMAIN, "ta:ClusterBody");		
    			modelbuilder.add("ta:labels", RDFS.RANGE, "ta:Label");
    			modelbuilder.add("ta:labels", RDFS.SUBPROPERTYOF, "as:tags");
    			
    			modelbuilder.add("ta:topRanked", RDF.TYPE, OWL.DATATYPEPROPERTY);		
    			modelbuilder.add("ta:topRanked", RDFS.RANGE, "xsd:boolean");*/
    			
    			int locid=1;
    			//uuid
    			for(int j=0; j<jarray.size(); j++) {
    				if(jarray.get(j).getAsJsonObject().has("id") && jarray.get(j).getAsJsonObject().has("labels")  && jarray.get(j).getAsJsonObject().has("tweets") && jarray.get(j).getAsJsonObject().has("top_ranked_tweets")) {
    			 	
	    				modelbuilder.add("ta:Cluster_Annotation_"+jarray.get(j).getAsJsonObject().get("id").getAsString(), RDF.TYPE, "oa:Annotation");
	        			modelbuilder.add("ta:Cluster_Annotation_"+jarray.get(j).getAsJsonObject().get("id").getAsString(), "oa:hasBody", "ta:ClusterBody_"+jarray.get(j).getAsJsonObject().get("id").getAsString());
	        			modelbuilder.add("ta:Cluster_Annotation_"+jarray.get(j).getAsJsonObject().get("id").getAsString(), "oa:hasTarget", "ta:Collection_"+jarray.get(j).getAsJsonObject().get("id").getAsString());
	        			
	        			
	        			for(int i=labels_sum-1;i<jarray.get(j).getAsJsonObject().get("labels").getAsJsonArray().size();i++) {
		    				
		    				modelbuilder.add("ta:Label_"+Integer.valueOf(i+1), RDF.TYPE, "ta:Label");
		        			modelbuilder.add("ta:Label_"+Integer.valueOf(i+1), "as:accuracy", jarray.get(j).getAsJsonObject().get("labels").getAsJsonArray().get(i).getAsJsonObject().get("score").getAsFloat());
		        			modelbuilder.add("ta:Label_"+Integer.valueOf(i+1), "as:name", jarray.get(j).getAsJsonObject().get("labels").getAsJsonArray().get(i).getAsJsonObject().get("text").getAsString());
		    			}
		    			labels_sum=labels_sum+jarray.get(j).getAsJsonObject().get("labels").getAsJsonArray().size();
		    			
		    			for(int i=tweets_sum-1;i<jarray.get(j).getAsJsonObject().get("tweets").getAsJsonArray().size();i++) {
		    				
		    				modelbuilder.add("ta:Annotation_"+Integer.valueOf(i+1), RDF.TYPE, "oa:Annotation");
		    				modelbuilder.add("ta:Annotation_"+Integer.valueOf(i+1), "oa:hasBody", "ta:Resource_"+Integer.valueOf(i+1));
		    				modelbuilder.add("ta:Annotation_"+Integer.valueOf(i+1), "oa:hasTarget", factory.createIRI("https://twitter.com/"+jarray.get(j).getAsJsonObject().get("tweets").getAsJsonArray().get(i).getAsString()));
		    				modelbuilder.add(factory.createIRI("https://twitter.com/"+jarray.get(j).getAsJsonObject().get("tweets").getAsJsonArray().get(i).getAsString()), RDF.TYPE, "ta:Tweet");
		    			
		    				modelbuilder.add("ta:Resource_"+Integer.valueOf(i+1), RDF.TYPE, RDFS.RESOURCE);
		    			
		        			
		        			istop=false;
		        			for(int k=0;k<jarray.get(j).getAsJsonObject().get("top_ranked_tweets").getAsJsonArray().size();k++) {
		        				if(jarray.get(j).getAsJsonObject().get("tweets").getAsJsonArray().get(i).getAsString().equals(jarray.get(j).getAsJsonObject().get("top_ranked_tweets").getAsJsonArray().get(k).getAsString())) {
		        					istop=true;
		        					break;
		        				}
		        			}
		        			modelbuilder.add("ta:Resource_"+Integer.valueOf(i+1), "ta:topRanked", istop);
		    			}
		    			tweets_sum=tweets_sum+jarray.get(j).getAsJsonObject().get("tweets").getAsJsonArray().size();
		    			
		    			modelbuilder.add("ta:Collection_"+jarray.get(j).getAsJsonObject().get("id").getAsString(), RDF.TYPE, "as:Collection");
		    			for(int i=t; i<t+jarray.get(j).getAsJsonObject().get("tweets").getAsJsonArray().size(); i++) {
		    				
		    				modelbuilder.add("ta:Collection_"+jarray.get(j).getAsJsonObject().get("id").getAsString(), "as:items", "ta:Annotation_"+Integer.valueOf(i+1));
		    			}
		    			
		    			modelbuilder.add("ta:ClusterBody_"+jarray.get(j).getAsJsonObject().get("id").getAsString(), RDF.TYPE, "ta:ClusterBody");
		    			for(int i=l;i<l+jarray.get(j).getAsJsonObject().get("labels").getAsJsonArray().size();i++) {
		    				
		    				modelbuilder.add("ta:ClusterBody_"+jarray.get(j).getAsJsonObject().get("id").getAsString(), "ta:labels", "ta:Label_"+Integer.valueOf(i+1));
		    				if(jarray.get(j).getAsJsonObject().has("location")) {

			        			modelbuilder.add("ta:ClusterBody_"+jarray.get(j).getAsJsonObject().get("id").getAsString(), "wgs84_pos:lat", Float.parseFloat(jarray.get(j).getAsJsonObject().get("location").getAsJsonObject().get("latitude").getAsString()));		
			        			modelbuilder.add("ta:ClusterBody_"+jarray.get(j).getAsJsonObject().get("id").getAsString(), "wgs84_pos:long", Float.parseFloat(jarray.get(j).getAsJsonObject().get("location").getAsJsonObject().get("longitude").getAsString()));
			    				locid++;
			    			}
		    			}	
		    			l=labels_sum-1;
		    			t=tweets_sum-1;	
    				}	    	
    			}
		    	Model model = modelbuilder.build();
		    	return model;
    		}		
    	return null;
    }
	
public static Model buildModel2(String jsonLine) throws JsonSyntaxException, JsonIOException, IOException, InterruptedException {
		
		boolean istop;
		String uuid = UUID.randomUUID().toString().replaceAll("-", "");
		int t=0;
		int l=0;
		int labels_sum=1;
		int tweets_sum=1;
		ModelBuilder modelbuilder = new ModelBuilder();
		// Write json input in a file and then read json file
		PrintWriter writer = new PrintWriter("json.json", "UTF-8");
    	writer.println(jsonLine);
    	writer.close();
    	Gson gson = new Gson();
    	JsonElement json = gson.fromJson(new FileReader("json.json"), JsonElement.class);
    	JsonArray jarray = json.getAsJsonArray();
    	ValueFactory factory = SimpleValueFactory.getInstance();
		
		if(jarray.size()!=0) {
    	   	
    			modelbuilder.setNamespace("as", Namespaces.AS).setNamespace("oa", Namespaces.OA).setNamespace("owl",Namespaces.OWL).setNamespace("rdf", Namespaces.RDF).
				setNamespace("rdfs", Namespaces.RDFS).setNamespace("ta", Namespaces.TA).setNamespace("tweets", Namespaces.TWEETS).setNamespace("xsd", Namespaces.XSD)
				.setNamespace("wgs84_pos", Namespaces.WGS84);
    			/*modelbuilder.add(Namespaces.TA, RDF.TYPE, OWL.ONTOLOGY);
    		
    			modelbuilder.add(Namespaces.TA, OWL.IMPORTS, factory.createIRI(Namespaces.OA.replace("#", "")));
    			modelbuilder.add(Namespaces.TA, OWL.IMPORTS, factory.createIRI("https://www.w3.org/ns/activitystreams-owl"));
    			modelbuilder.add(Namespaces.TA, OWL.VERSIONINFO, "Created with TopBraid Composer");
    			
    			modelbuilder.add("ta:ClusterBody", RDF.TYPE, OWL.CLASS);
    			modelbuilder.add("ta:ClusterBody", RDFS.SUBCLASSOF, "as:Object");
    			
    			modelbuilder.add("ta:Label", RDF.TYPE, OWL.CLASS);
    			modelbuilder.add("ta:Label", RDFS.SUBCLASSOF, "as:Object");
    			
    			modelbuilder.add("ta:Tweet", RDF.TYPE, OWL.CLASS);
    			modelbuilder.add("ta:Tweet", RDFS.SUBCLASSOF, "as:Note");
    			
    			modelbuilder.add("ta:labels", RDF.TYPE, OWL.OBJECTPROPERTY);
    			modelbuilder.add("ta:labels", RDFS.DOMAIN, "ta:ClusterBody");		
    			modelbuilder.add("ta:labels", RDFS.RANGE, "ta:Label");
    			modelbuilder.add("ta:labels", RDFS.SUBPROPERTYOF, "as:tags");
    			
    			modelbuilder.add("ta:topRanked", RDF.TYPE, OWL.DATATYPEPROPERTY);		
    			modelbuilder.add("ta:topRanked", RDFS.RANGE, "xsd:boolean");*/
    			
    			int locid=1;
    			//uuid
    			for(int j=0; j<jarray.size(); j++) {
    				if(jarray.get(j).getAsJsonObject().has("id") && jarray.get(j).getAsJsonObject().has("labels")  && jarray.get(j).getAsJsonObject().has("tweets") && jarray.get(j).getAsJsonObject().has("top_ranked_tweets")) {
    			 	
	    				modelbuilder.add("ta:Cluster_Annotation_"+uuid, RDF.TYPE, "oa:Annotation");
	        			modelbuilder.add("ta:Cluster_Annotation_"+uuid, "oa:hasBody", "ta:ClusterBody_"+uuid);
	        			modelbuilder.add("ta:Cluster_Annotation_"+uuid, "oa:hasTarget", "ta:Collection_"+uuid);
	        			
	        			
	        			for(int i=labels_sum-1;i<jarray.get(j).getAsJsonObject().get("labels").getAsJsonArray().size();i++) {
		    				
		    				modelbuilder.add("ta:Label_"+uuid+Integer.valueOf(i+1), RDF.TYPE, "ta:Label");
		        			modelbuilder.add("ta:Label_"+uuid+Integer.valueOf(i+1), "as:accuracy", jarray.get(j).getAsJsonObject().get("labels").getAsJsonArray().get(i).getAsJsonObject().get("score").getAsFloat());
		        			modelbuilder.add("ta:Label_"+uuid+Integer.valueOf(i+1), "as:name", jarray.get(j).getAsJsonObject().get("labels").getAsJsonArray().get(i).getAsJsonObject().get("text").getAsString());
		    			
		        			JSONObject babel_res = new JSONObject ();
		        			babel_res=calculateBabel(jarray.get(j).getAsJsonObject().get("labels").getAsJsonArray().get(i).getAsJsonObject().get("text").getAsString());
		        			
		        			modelbuilder.add("ta:Label_"+uuid+Integer.valueOf(i+1), "ta:hasBabel", "ta:BabelEntity_"+uuid+Integer.valueOf(i+1));
		        			modelbuilder.add("ta:BabelEntity_"+uuid+Integer.valueOf(i+1), RDF.TYPE, "ta:BabelEntity");
		        			modelbuilder.add("ta:BabelEntity_"+uuid+Integer.valueOf(i+1), "ta:level1", babel_res.get("Level 1"));
		        			modelbuilder.add("ta:BabelEntity_"+uuid+Integer.valueOf(i+1), "ta:level2", babel_res.get("Level 2"));
		        			modelbuilder.add("ta:BabelEntity_"+uuid+Integer.valueOf(i+1), "ta:level3", babel_res.get("Level 3"));
			    			
	        			
	        			}
		    			labels_sum=labels_sum+jarray.get(j).getAsJsonObject().get("labels").getAsJsonArray().size();
		    			
		    			for(int i=tweets_sum-1;i<jarray.get(j).getAsJsonObject().get("tweets").getAsJsonArray().size();i++) {
		    				
		    				modelbuilder.add("ta:Annotation_"+uuid+Integer.valueOf(i+1), RDF.TYPE, "oa:Annotation");
		    				modelbuilder.add("ta:Annotation_"+uuid+Integer.valueOf(i+1), "oa:hasBody", "ta:Resource_"+uuid+Integer.valueOf(i+1));
		    				modelbuilder.add("ta:Annotation_"+uuid+Integer.valueOf(i+1), "oa:hasTarget", factory.createIRI("https://twitter.com/"+jarray.get(j).getAsJsonObject().get("tweets").getAsJsonArray().get(i).getAsString()));
		    				modelbuilder.add(factory.createIRI("https://twitter.com/"+jarray.get(j).getAsJsonObject().get("tweets").getAsJsonArray().get(i).getAsString()), RDF.TYPE, "ta:Tweet");
		    			
		    				modelbuilder.add("ta:Resource_"+uuid+Integer.valueOf(i+1), RDF.TYPE, RDFS.RESOURCE);
		    			
		        			
		        			istop=false;
		        			for(int k=0;k<jarray.get(j).getAsJsonObject().get("top_ranked_tweets").getAsJsonArray().size();k++) {
		        				if(jarray.get(j).getAsJsonObject().get("tweets").getAsJsonArray().get(i).getAsString().equals(jarray.get(j).getAsJsonObject().get("top_ranked_tweets").getAsJsonArray().get(k).getAsString())) {
		        					istop=true;
		        					break;
		        				}
		        			}
		        			modelbuilder.add("ta:Resource_"+uuid+Integer.valueOf(i+1), "ta:topRanked", istop);
		    			}
		    			tweets_sum=tweets_sum+jarray.get(j).getAsJsonObject().get("tweets").getAsJsonArray().size();
		    			
		    			modelbuilder.add("ta:Collection_"+uuid, RDF.TYPE, "as:Collection");
		    			for(int i=t; i<t+jarray.get(j).getAsJsonObject().get("tweets").getAsJsonArray().size(); i++) {
		    				
		    				modelbuilder.add("ta:Collection_"+uuid, "as:items", "ta:Annotation_"+uuid+Integer.valueOf(i+1));
		    			}
		    			
		    			modelbuilder.add("ta:ClusterBody_"+uuid, RDF.TYPE, "ta:ClusterBody");
		    			for(int i=l;i<l+jarray.get(j).getAsJsonObject().get("labels").getAsJsonArray().size();i++) {
		    				
		    				modelbuilder.add("ta:ClusterBody_"+uuid, "ta:labels", "ta:Label_"+uuid+Integer.valueOf(i+1));
		    				if(jarray.get(j).getAsJsonObject().has("location")) {

			        			modelbuilder.add("ta:ClusterBody_"+uuid, "wgs84_pos:lat", Float.parseFloat(jarray.get(j).getAsJsonObject().get("location").getAsJsonObject().get("latitude").getAsString()));		
			        			modelbuilder.add("ta:ClusterBody_"+uuid, "wgs84_pos:long", Float.parseFloat(jarray.get(j).getAsJsonObject().get("location").getAsJsonObject().get("longitude").getAsString()));
			    				locid++;
			    			}
		    			}	
		    			
		    			/*modelbuilder.add("ta:ClusterBody_"+jarray.get(j).getAsJsonObject().get("id").getAsString(), RDF.TYPE, "ta:ClusterBody");
		    			for(int i=l;i<l+jarray.get(j).getAsJsonObject().get("labels").getAsJsonArray().size();i++) {
		    				
		    				modelbuilder.add("ta:ClusterBody_"+jarray.get(j).getAsJsonObject().get("id").getAsString(), "ta:labels", "ta:Label_"+Integer.valueOf(i+1));
		    				
		    				if(jarray.get(j).getAsJsonObject().has("location")) {

		    					//RETURN TO OLD!!
			    				modelbuilder.add("ta:ClusterBody_"+uuid, "ta:hasPoint", "ta:Point_"+uuid);
			    				modelbuilder.add("ta:Point_"+uuid, RDF.TYPE, "ogc:Geometry");
			    				String point="<http://www.opengis.net/def/crs/OGC/1.3/CRS84> POINT(";
			    				point=point+ Float.parseFloat(jarray.get(j).getAsJsonObject().get("location").getAsJsonObject().get("latitude").getAsString())+" "+ Float.parseFloat(jarray.get(j).getAsJsonObject().get("location").getAsJsonObject().get("longitude").getAsString());
			    				point=point+")^^ogc:wktLiteral";
			    				modelbuilder.add("ta:Point_"+uuid, "ogc:asWKT", point);
			        			//modelbuilder.add("ta:ClusterBody_"+jarray.get(j).getAsJsonObject().get("id").getAsString(), "wgs84_pos:lat", Float.parseFloat(jarray.get(j).getAsJsonObject().get("location").getAsJsonObject().get("latitude").getAsString()));		
			        			//modelbuilder.add("ta:ClusterBody_"+jarray.get(j).getAsJsonObject().get("id").getAsString(), "wgs84_pos:long", Float.parseFloat(jarray.get(j).getAsJsonObject().get("location").getAsJsonObject().get("longitude").getAsString()));
			    				locid++;
			    				
			    				//<http://www.opengis.net/def/crs/OGC/1.3/CRS84> POINT(-122.4192 37.7793)”^^ogc:wktLiteral
			    				

			    			}
		    			}*/	
		    			l=labels_sum-1;
		    			t=tweets_sum-1;	
    				}	    	
    			}
		    	Model model = modelbuilder.build();
		    	return model;
    		}		
    	return null;
    }
public static Model buildModel_final(String jsonLine) throws JsonSyntaxException, JsonIOException, IOException, InterruptedException {
	
	boolean istop;
	String uuid = UUID.randomUUID().toString().replaceAll("-", "");
	int t=0;
	int l=0;
	int labels_sum=1;
	int tweets_sum=1;
	ModelBuilder modelbuilder = new ModelBuilder();
	// Write json input in a file and then read json file
	PrintWriter writer = new PrintWriter("json.json", "UTF-8");
	writer.println(jsonLine);
	writer.close();
	Gson gson = new Gson();
	JsonElement json = gson.fromJson(new FileReader("json.json"), JsonElement.class);
	//JsonArray jarray = json.getAsJsonArray();
	JsonObject jobj = json.getAsJsonObject();
	ValueFactory factory = SimpleValueFactory.getInstance();
	
	//if(jarray.size()!=0) {
	   	
			modelbuilder.setNamespace("as", Namespaces.AS).setNamespace("oa", Namespaces.OA).setNamespace("owl",Namespaces.OWL).setNamespace("rdf", Namespaces.RDF).
			setNamespace("rdfs", Namespaces.RDFS).setNamespace("ta", Namespaces.TA).setNamespace("tweets", Namespaces.TWEETS).setNamespace("xsd", Namespaces.XSD)
			.setNamespace("wgs84_pos", Namespaces.WGS84).setNamespace("time", "https://www.w3.org/2006/time#");
			/*modelbuilder.add(Namespaces.TA, RDF.TYPE, OWL.ONTOLOGY);
		
			modelbuilder.add(Namespaces.TA, OWL.IMPORTS, factory.createIRI(Namespaces.OA.replace("#", "")));
			modelbuilder.add(Namespaces.TA, OWL.IMPORTS, factory.createIRI("https://www.w3.org/ns/activitystreams-owl"));
			modelbuilder.add(Namespaces.TA, OWL.VERSIONINFO, "Created with TopBraid Composer");
			
			modelbuilder.add("ta:ClusterBody", RDF.TYPE, OWL.CLASS);
			modelbuilder.add("ta:ClusterBody", RDFS.SUBCLASSOF, "as:Object");
			
			modelbuilder.add("ta:Label", RDF.TYPE, OWL.CLASS);
			modelbuilder.add("ta:Label", RDFS.SUBCLASSOF, "as:Object");
			
			modelbuilder.add("ta:Tweet", RDF.TYPE, OWL.CLASS);
			modelbuilder.add("ta:Tweet", RDFS.SUBCLASSOF, "as:Note");
			
			modelbuilder.add("ta:labels", RDF.TYPE, OWL.OBJECTPROPERTY);
			modelbuilder.add("ta:labels", RDFS.DOMAIN, "ta:ClusterBody");		
			modelbuilder.add("ta:labels", RDFS.RANGE, "ta:Label");
			modelbuilder.add("ta:labels", RDFS.SUBPROPERTYOF, "as:tags");
			
			modelbuilder.add("ta:topRanked", RDF.TYPE, OWL.DATATYPEPROPERTY);		
			modelbuilder.add("ta:topRanked", RDFS.RANGE, "xsd:boolean");*/
			
			int locid=1;
			
			//uuid
			//for(int j=0; j<jarray.size(); j++) {
				//if(jarray.get(j).getAsJsonObject().has("id") && jarray.get(j).getAsJsonObject().has("labels")  && jarray.get(j).getAsJsonObject().has("tweets") && jarray.get(j).getAsJsonObject().has("top_ranked_tweets")) {
			 	
    				modelbuilder.add("ta:Cluster_Annotation_"+uuid, RDF.TYPE, "oa:Annotation");
        			modelbuilder.add("ta:Cluster_Annotation_"+uuid, "oa:hasBody", "ta:ClusterBody_"+uuid);
        			modelbuilder.add("ta:Cluster_Annotation_"+uuid, "oa:hasTarget", "ta:Collection_"+uuid);
        			
        			modelbuilder.add("ta:ClusterBody_"+uuid, RDF.TYPE, "ta:ClusterBody");
        			modelbuilder.add("ta:ClusterBody_"+uuid, "time:inXSDDateTimeStamp", jobj.get("timestamp"));
        			modelbuilder.add("ta:ClusterBody_"+uuid, "ta:hasUseCase", jobj.get("usecase"));
        			modelbuilder.add("ta:ClusterBody_"+uuid, "ta:hasLanguage", jobj.get("language"));
        			
        			int counter=0; 
        			int counter2=0;
        			modelbuilder.add("ta:Collection_"+uuid, RDF.TYPE, "as:Collection");
		    		
        			for(int i=0;i<jobj.get("topics").getAsJsonArray().size();i++) {
        			//	String uuid2 = UUID.randomUUID().toString().replaceAll("-", "");
        				modelbuilder.add("ta:Collection_"+uuid, "ta:hasTopics", "ta:Topic_"+uuid+i);
        				modelbuilder.add("ta:Topic_"+uuid+i, RDF.TYPE, "ta:Topic");
        				modelbuilder.add("ta:Topic_"+uuid+i, "ta:hasId", jobj.get("topics").getAsJsonArray().get(i).getAsJsonObject().get("id"));
        					
        					
        				
        				for(int j=0;j<jobj.get("topics").getAsJsonArray().get(i).getAsJsonObject().get("labels").getAsJsonArray().size();j++) {
        					counter++;
        					modelbuilder.add("ta:Topic_"+uuid+i, "ta:labels", "ta:Label_"+uuid+counter);
        				
		    				modelbuilder.add("ta:Label_"+uuid+counter, RDF.TYPE, "ta:Label");
		        			modelbuilder.add("ta:Label_"+uuid+counter, "as:accuracy", jobj.get("topics").getAsJsonArray().get(i).getAsJsonObject().get("labels").getAsJsonArray().get(j).getAsJsonObject().get("score").getAsFloat());
		        			modelbuilder.add("ta:Label_"+uuid+counter, "as:name", jobj.get("topics").getAsJsonArray().get(i).getAsJsonObject().get("labels").getAsJsonArray().get(j).getAsJsonObject().get("text").getAsString());
		    			
		        			JSONObject babel_res = new JSONObject ();
		        			babel_res=calculateBabel(jobj.get("topics").getAsJsonArray().get(i).getAsJsonObject().get("labels").getAsJsonArray().get(j).getAsJsonObject().get("text").getAsString());
		        			
		        			modelbuilder.add("ta:Label_"+uuid+counter, "ta:hasBabel", "ta:BabelEntity_"+uuid+counter);
		        			modelbuilder.add("ta:BabelEntity_"+uuid+counter, RDF.TYPE, "ta:BabelEntity");
		        			if(babel_res.containsKey("Level 1")) {
		        				modelbuilder.add("ta:BabelEntity_"+uuid+counter, "ta:level1", babel_res.get("Level 1"));
		        			}
		        			else {
		        				modelbuilder.add("ta:BabelEntity_"+uuid+counter, "ta:level1", "null");
		        			}
		        			if(babel_res.containsKey("Level 2")) {
		        			modelbuilder.add("ta:BabelEntity_"+uuid+counter, "ta:level2", babel_res.get("Level 2"));
		        			}
		        			else {
		        				modelbuilder.add("ta:BabelEntity_"+uuid+counter, "ta:level2", "null");
		        			}
		        			if(babel_res.containsKey("Level 3")) {
		        			modelbuilder.add("ta:BabelEntity_"+uuid+counter, "ta:level3", babel_res.get("Level 3"));
		        			}
		        			else {
		        				modelbuilder.add("ta:BabelEntity_"+uuid+counter, "ta:level3", "null");
		        			}
        				}
		    			for(int j=0; j<jobj.get("topics").getAsJsonArray().get(i).getAsJsonObject().get("tweets").getAsJsonArray().size();j++) {
		    				counter2++;
		    				modelbuilder.add("ta:Tweet_"+uuid+counter2, RDF.TYPE, "ta:Tweet");
		    				
		    				modelbuilder.add("ta:Tweet_"+uuid+counter2, "ta:hasUrl", factory.createIRI("https://twitter.com/"+jobj.get("topics").getAsJsonArray().get(i).getAsJsonObject().get("tweets").getAsJsonArray().get(j).getAsString()));
		    				
		    			
		        			
		        			istop=false;
		        			for(int k=0;k<jobj.get("topics").getAsJsonArray().get(i).getAsJsonObject().get("top_ranked_tweets").getAsJsonArray().size();k++) {
		        				if(jobj.get("topics").getAsJsonArray().get(i).getAsJsonObject().get("tweets").getAsJsonArray().get(j).getAsString().equals(jobj.get("topics").getAsJsonArray().get(i).getAsJsonObject().get("top_ranked_tweets").getAsJsonArray().get(k).getAsString())) {
		        					istop=true;
		        					break;
		        				}
		        			}
		        			modelbuilder.add("ta:Tweet_"+uuid+counter2, "ta:topRanked", istop);
		        			modelbuilder.add("ta:Topic_"+uuid+i, "as:items", "ta:Tweet_"+uuid+counter2);
		    			}
        			
        			}
	    		//	labels_sum=labels_sum+jarray.get(j).getAsJsonObject().get("labels").getAsJsonArray().size();
	    			
	    			
	    			
	    		//	}	
	    			
	    			/*modelbuilder.add("ta:ClusterBody_"+jarray.get(j).getAsJsonObject().get("id").getAsString(), RDF.TYPE, "ta:ClusterBody");
	    			for(int i=l;i<l+jarray.get(j).getAsJsonObject().get("labels").getAsJsonArray().size();i++) {
	    				
	    				modelbuilder.add("ta:ClusterBody_"+jarray.get(j).getAsJsonObject().get("id").getAsString(), "ta:labels", "ta:Label_"+Integer.valueOf(i+1));
	    				
	    				if(jarray.get(j).getAsJsonObject().has("location")) {

	    					//RETURN TO OLD!!
		    				modelbuilder.add("ta:ClusterBody_"+uuid, "ta:hasPoint", "ta:Point_"+uuid);
		    				modelbuilder.add("ta:Point_"+uuid, RDF.TYPE, "ogc:Geometry");
		    				String point="<http://www.opengis.net/def/crs/OGC/1.3/CRS84> POINT(";
		    				point=point+ Float.parseFloat(jarray.get(j).getAsJsonObject().get("location").getAsJsonObject().get("latitude").getAsString())+" "+ Float.parseFloat(jarray.get(j).getAsJsonObject().get("location").getAsJsonObject().get("longitude").getAsString());
		    				point=point+")^^ogc:wktLiteral";
		    				modelbuilder.add("ta:Point_"+uuid, "ogc:asWKT", point);
		        			//modelbuilder.add("ta:ClusterBody_"+jarray.get(j).getAsJsonObject().get("id").getAsString(), "wgs84_pos:lat", Float.parseFloat(jarray.get(j).getAsJsonObject().get("location").getAsJsonObject().get("latitude").getAsString()));		
		        			//modelbuilder.add("ta:ClusterBody_"+jarray.get(j).getAsJsonObject().get("id").getAsString(), "wgs84_pos:long", Float.parseFloat(jarray.get(j).getAsJsonObject().get("location").getAsJsonObject().get("longitude").getAsString()));
		    				locid++;
		    				
		    				//<http://www.opengis.net/def/crs/OGC/1.3/CRS84> POINT(-122.4192 37.7793)”^^ogc:wktLiteral
		    				

		    			}
	    			}*/	
	    			//l=labels_sum-1;
	    			//t=tweets_sum-1;	
				//}	    	
			
	    	Model model = modelbuilder.build();
	    	return model;
				
	//return null;
}
public static Model buildModel3(String jsonLine) throws JsonSyntaxException, JsonIOException, IOException, InterruptedException {
	
	boolean istop;
	String uuid = UUID.randomUUID().toString().replaceAll("-", "");
	int t=0;
	int l=0;
	int labels_sum=1;
	int tweets_sum=1;
	ModelBuilder modelbuilder = new ModelBuilder();
	// Write json input in a file and then read json file
	PrintWriter writer = new PrintWriter("json.json", "UTF-8");
	writer.println(jsonLine);
	writer.close();
	Gson gson = new Gson();
	JsonElement json = gson.fromJson(new FileReader("json.json"), JsonElement.class);
	//JsonArray jarray = json.getAsJsonArray();
	JsonObject jobj = json.getAsJsonObject();
	ValueFactory factory = SimpleValueFactory.getInstance();
	
	//if(jarray.size()!=0) {
	   	
			modelbuilder.setNamespace("as", Namespaces.AS).setNamespace("oa", Namespaces.OA).setNamespace("owl",Namespaces.OWL).setNamespace("rdf", Namespaces.RDF).
			setNamespace("rdfs", Namespaces.RDFS).setNamespace("ta", Namespaces.TA).setNamespace("tweets", Namespaces.TWEETS).setNamespace("xsd", Namespaces.XSD)
			.setNamespace("wgs84_pos", Namespaces.WGS84).setNamespace("time", "https://www.w3.org/2006/time#");
			/*modelbuilder.add(Namespaces.TA, RDF.TYPE, OWL.ONTOLOGY);
		
			modelbuilder.add(Namespaces.TA, OWL.IMPORTS, factory.createIRI(Namespaces.OA.replace("#", "")));
			modelbuilder.add(Namespaces.TA, OWL.IMPORTS, factory.createIRI("https://www.w3.org/ns/activitystreams-owl"));
			modelbuilder.add(Namespaces.TA, OWL.VERSIONINFO, "Created with TopBraid Composer");
			
			modelbuilder.add("ta:ClusterBody", RDF.TYPE, OWL.CLASS);
			modelbuilder.add("ta:ClusterBody", RDFS.SUBCLASSOF, "as:Object");
			
			modelbuilder.add("ta:Label", RDF.TYPE, OWL.CLASS);
			modelbuilder.add("ta:Label", RDFS.SUBCLASSOF, "as:Object");
			
			modelbuilder.add("ta:Tweet", RDF.TYPE, OWL.CLASS);
			modelbuilder.add("ta:Tweet", RDFS.SUBCLASSOF, "as:Note");
			
			modelbuilder.add("ta:labels", RDF.TYPE, OWL.OBJECTPROPERTY);
			modelbuilder.add("ta:labels", RDFS.DOMAIN, "ta:ClusterBody");		
			modelbuilder.add("ta:labels", RDFS.RANGE, "ta:Label");
			modelbuilder.add("ta:labels", RDFS.SUBPROPERTYOF, "as:tags");
			
			modelbuilder.add("ta:topRanked", RDF.TYPE, OWL.DATATYPEPROPERTY);		
			modelbuilder.add("ta:topRanked", RDFS.RANGE, "xsd:boolean");*/
			
			int locid=1;
			
			//uuid
			//for(int j=0; j<jarray.size(); j++) {
				//if(jarray.get(j).getAsJsonObject().has("id") && jarray.get(j).getAsJsonObject().has("labels")  && jarray.get(j).getAsJsonObject().has("tweets") && jarray.get(j).getAsJsonObject().has("top_ranked_tweets")) {
			 	
    				modelbuilder.add("ta:Cluster_Annotation_"+uuid, RDF.TYPE, "oa:Annotation");
        			modelbuilder.add("ta:Cluster_Annotation_"+uuid, "oa:hasBody", "ta:ClusterBody_"+uuid);
        			modelbuilder.add("ta:Cluster_Annotation_"+uuid, "oa:hasTarget", "ta:Collection_"+uuid);
        			modelbuilder.add("ta:Cluster_Annotation_"+uuid, "time:inXSDDateTimeStamp", jobj.get("timestamp"));
        			modelbuilder.add("ta:Cluster_Annotation_"+uuid, "ta:hasUseCase", jobj.get("usecase"));
        			modelbuilder.add("ta:Cluster_Annotation_"+uuid, "ta:hasLanguage", jobj.get("language"));
        			
        			int counter=0; 
        			int counter2=0;
        			for(int i=0;i<jobj.get("topics").getAsJsonArray().size();i++) {
        			//	String uuid2 = UUID.randomUUID().toString().replaceAll("-", "");
        				modelbuilder.add("ta:ClusterBody_"+uuid, "ta:hasCluster", "ta:Cluster_"+uuid+i);
        				modelbuilder.add("ta:Cluster_"+uuid+i, "ta:hasId", jobj.get("topics").getAsJsonArray().get(i).getAsJsonObject().get("id"));
        					
        					
        				
        				for(int j=0;j<jobj.get("topics").getAsJsonArray().get(i).getAsJsonObject().get("labels").getAsJsonArray().size();j++) {
        					counter++;
        					modelbuilder.add("ta:Cluster_"+uuid+i, "ta:labels", "ta:Label_"+uuid+counter);
        				
		    				modelbuilder.add("ta:Label_"+uuid+counter, RDF.TYPE, "ta:Label");
		        			modelbuilder.add("ta:Label_"+uuid+counter, "as:accuracy", jobj.get("topics").getAsJsonArray().get(i).getAsJsonObject().get("labels").getAsJsonArray().get(j).getAsJsonObject().get("score").getAsFloat());
		        			modelbuilder.add("ta:Label_"+uuid+counter, "as:name", jobj.get("topics").getAsJsonArray().get(i).getAsJsonObject().get("labels").getAsJsonArray().get(j).getAsJsonObject().get("text").getAsString());
		    			
		        			JSONObject babel_res = new JSONObject ();
		        			babel_res=calculateBabel(jobj.get("topics").getAsJsonArray().get(i).getAsJsonObject().get("labels").getAsJsonArray().get(j).getAsJsonObject().get("text").getAsString());
		        			
		        			modelbuilder.add("ta:Label_"+uuid+counter, "ta:hasBabel", "ta:BabelEntity_"+uuid+counter);
		        			modelbuilder.add("ta:BabelEntity_"+uuid+counter, RDF.TYPE, "ta:BabelEntity");
		        			if(babel_res.containsKey("Level 1")) {
		        				modelbuilder.add("ta:BabelEntity_"+uuid+counter, "ta:level1", babel_res.get("Level 1"));
		        			}
		        			else {
		        				modelbuilder.add("ta:BabelEntity_"+uuid+counter, "ta:level1", "null");
		        			}
		        			if(babel_res.containsKey("Level 2")) {
		        			modelbuilder.add("ta:BabelEntity_"+uuid+counter, "ta:level2", babel_res.get("Level 2"));
		        			}
		        			else {
		        				modelbuilder.add("ta:BabelEntity_"+uuid+counter, "ta:level2", "null");
		        			}
		        			if(babel_res.containsKey("Level 3")) {
		        			modelbuilder.add("ta:BabelEntity_"+uuid+counter, "ta:level3", babel_res.get("Level 3"));
		        			}
		        			else {
		        				modelbuilder.add("ta:BabelEntity_"+uuid+counter, "ta:level3", "null");
		        			}
        				}
		    			for(int j=0; j<jobj.get("topics").getAsJsonArray().get(i).getAsJsonObject().get("tweets").getAsJsonArray().size();j++) {
		    				counter2++;
		    				modelbuilder.add("ta:Annotation_"+uuid+counter2, RDF.TYPE, "oa:Annotation");
		    				modelbuilder.add("ta:Annotation_"+uuid+counter2, "oa:hasBody", "ta:Resource_"+uuid+counter2);
		    				modelbuilder.add("ta:Annotation_"+uuid+counter2, "oa:hasTarget", factory.createIRI("https://twitter.com/"+jobj.get("topics").getAsJsonArray().get(i).getAsJsonObject().get("tweets").getAsJsonArray().get(j).getAsString()));
		    				modelbuilder.add(factory.createIRI("https://twitter.com/"+jobj.get("topics").getAsJsonArray().get(i).getAsJsonObject().get("tweets").getAsJsonArray().get(j).getAsString()), RDF.TYPE, "ta:Tweet");
			    			
		    			
		    				modelbuilder.add("ta:Resource_"+uuid+counter2, RDF.TYPE, RDFS.RESOURCE);
		    			
		        			
		        			istop=false;
		        			for(int k=0;k<jobj.get("topics").getAsJsonArray().get(i).getAsJsonObject().get("top_ranked_tweets").getAsJsonArray().size();k++) {
		        				if(jobj.get("topics").getAsJsonArray().get(i).getAsJsonObject().get("tweets").getAsJsonArray().get(j).getAsString().equals(jobj.get("topics").getAsJsonArray().get(i).getAsJsonObject().get("top_ranked_tweets").getAsJsonArray().get(k).getAsString())) {
		        					istop=true;
		        					break;
		        				}
		        			}
		        			modelbuilder.add("ta:Resource_"+uuid+counter2, "ta:topRanked", istop);
		        			modelbuilder.add("ta:Collection_"+uuid, "as:items", "ta:Annotation_"+uuid+counter2);
		    			}
        			
        			}
	    		//	labels_sum=labels_sum+jarray.get(j).getAsJsonObject().get("labels").getAsJsonArray().size();
	    			
	    			
	    			
	    		//	}	
	    			
	    			/*modelbuilder.add("ta:ClusterBody_"+jarray.get(j).getAsJsonObject().get("id").getAsString(), RDF.TYPE, "ta:ClusterBody");
	    			for(int i=l;i<l+jarray.get(j).getAsJsonObject().get("labels").getAsJsonArray().size();i++) {
	    				
	    				modelbuilder.add("ta:ClusterBody_"+jarray.get(j).getAsJsonObject().get("id").getAsString(), "ta:labels", "ta:Label_"+Integer.valueOf(i+1));
	    				
	    				if(jarray.get(j).getAsJsonObject().has("location")) {

	    					//RETURN TO OLD!!
		    				modelbuilder.add("ta:ClusterBody_"+uuid, "ta:hasPoint", "ta:Point_"+uuid);
		    				modelbuilder.add("ta:Point_"+uuid, RDF.TYPE, "ogc:Geometry");
		    				String point="<http://www.opengis.net/def/crs/OGC/1.3/CRS84> POINT(";
		    				point=point+ Float.parseFloat(jarray.get(j).getAsJsonObject().get("location").getAsJsonObject().get("latitude").getAsString())+" "+ Float.parseFloat(jarray.get(j).getAsJsonObject().get("location").getAsJsonObject().get("longitude").getAsString());
		    				point=point+")^^ogc:wktLiteral";
		    				modelbuilder.add("ta:Point_"+uuid, "ogc:asWKT", point);
		        			//modelbuilder.add("ta:ClusterBody_"+jarray.get(j).getAsJsonObject().get("id").getAsString(), "wgs84_pos:lat", Float.parseFloat(jarray.get(j).getAsJsonObject().get("location").getAsJsonObject().get("latitude").getAsString()));		
		        			//modelbuilder.add("ta:ClusterBody_"+jarray.get(j).getAsJsonObject().get("id").getAsString(), "wgs84_pos:long", Float.parseFloat(jarray.get(j).getAsJsonObject().get("location").getAsJsonObject().get("longitude").getAsString()));
		    				locid++;
		    				
		    				//<http://www.opengis.net/def/crs/OGC/1.3/CRS84> POINT(-122.4192 37.7793)”^^ogc:wktLiteral
		    				

		    			}
	    			}*/	
	    			//l=labels_sum-1;
	    			//t=tweets_sum-1;	
				//}	    	
			
	    	Model model = modelbuilder.build();
	    	return model;
				
	//return null;
}
	
	public static Model buildEventsModel(String jsonLine) throws JsonSyntaxException, JsonIOException, IOException {
		
		
		ModelBuilder modelbuilder = new ModelBuilder();
		// Write json input in a file and then read json file
		PrintWriter writer = new PrintWriter("json.json", "UTF-8");
    	writer.println(jsonLine);
    	writer.close();
    	Gson gson = new Gson();
    	JsonElement json = gson.fromJson(new FileReader("json.json"), JsonElement.class);
    	System.out.println("JSON"+json);
    	if(json.getAsJsonObject().has("events")) {
    		System.out.println("has");
    	}
    	JsonArray jarray = json.getAsJsonObject().get("events").getAsJsonArray();
    	System.out.println("JARRAY"+jarray);
    	ValueFactory factory = SimpleValueFactory.getInstance();
    	String newstring, location, eventtype;
		if(jarray.size()!=0) {
    	   	
    			modelbuilder.setNamespace("as", Namespaces.AS).setNamespace("oa", Namespaces.OA).setNamespace("owl",Namespaces.OWL).setNamespace("rdf", Namespaces.RDF).
				setNamespace("rdfs", Namespaces.RDFS).setNamespace("ta", Namespaces.TA).setNamespace("tweets", Namespaces.TWEETS).setNamespace("xsd", Namespaces.XSD)
				.setNamespace("wgs84_pos", Namespaces.WGS84);
    			/*modelbuilder.add(Namespaces.TA, RDF.TYPE, OWL.ONTOLOGY);
    		
    			modelbuilder.add(Namespaces.TA, OWL.IMPORTS, factory.createIRI(Namespaces.OA.replace("#", "")));
    			modelbuilder.add(Namespaces.TA, OWL.IMPORTS, factory.createIRI("https://www.w3.org/ns/activitystreams-owl"));
    			
    			modelbuilder.add("ta:EventBody", RDF.TYPE, OWL.CLASS);
    			modelbuilder.add("ta:EventBody", RDFS.SUBCLASSOF, "as:Object");
    			
    			modelbuilder.add("ta:EventTarget", RDF.TYPE, OWL.CLASS);
    			modelbuilder.add("ta:EventTarget", RDFS.SUBCLASSOF, "as:Object");
    			
    			modelbuilder.add("ta:Event", RDF.TYPE, OWL.CLASS);
    			modelbuilder.add("ta:Event", RDFS.SUBCLASSOF, "as:Object");
    			
    			modelbuilder.add("ta:EventFloods", RDF.TYPE, OWL.CLASS);
    			modelbuilder.add("ta:EventFloods", RDFS.SUBCLASSOF, "ta:Event");
    			
    			modelbuilder.add("ta:EventFood", RDF.TYPE, OWL.CLASS);
    			modelbuilder.add("ta:EventFood", RDFS.SUBCLASSOF, "ta:Event");
    			
    			modelbuilder.add("ta:EventSnow", RDF.TYPE, OWL.CLASS);
    			modelbuilder.add("ta:EventSnow", RDFS.SUBCLASSOF, "ta:Event");
    			
    			/*modelbuilder.add("ta:Tweet", RDF.TYPE, OWL.CLASS);
    			modelbuilder.add("ta:Tweet", RDFS.SUBCLASSOF, "as:Note");
    			
    			modelbuilder.add("ta:labels", RDF.TYPE, OWL.OBJECTPROPERTY);
    			modelbuilder.add("ta:labels", RDFS.DOMAIN, "ta:ClusterBody");		
    			modelbuilder.add("ta:labels", RDFS.RANGE, "ta:Label");
    			mocounteruilder.add("ta:labels", RDFS.SUBPROPERTYOF, "as:tags");
    			
    			modelbuilder.add("ta:topRanked", RDF.TYPE, OWL.DATATYPEPROPERTY);		
    			modelbuilder.add("ta:topRanked", RDFS.RANGE, "xsd:boolean");*/
    		
    			String[] data = new String [2];
    			for(int j=0; j<jarray.size(); j++) {
    				if(jarray.get(j).getAsJsonObject().has("useCase") && jarray.get(j).getAsJsonObject().has("score")  && jarray.get(j).getAsJsonObject().has("change") && jarray.get(j).getAsJsonObject().has("epoch_timestamp")) {
    					String uuid = UUID.randomUUID().toString().replaceAll("-", "");
	    				newstring=splitCamelCase(jarray.get(j).getAsJsonObject().get("useCase").getAsString());
	        			String[] words = newstring.split("\\s+");
	        			//System.out.println("111:"+Arrays.toString(newstring.split("\\s(?=[A-Z])")));
	        			//System.out.println("112:"+Arrays.toString(newstring.split("\\s(?=[A-Z])")).length());
	        			if(words.length==2) {
	        				location=words[0];
	        				eventtype=words[1];
	        				System.out.print("location:"+location+", event:"+eventtype+"\n");
	        				modelbuilder.add("ta:Event_Annotation_"+uuid, RDF.TYPE, "ta:Event"+eventtype);
	        				modelbuilder.add("ta:Event_Annotation_"+uuid, "ta:location", location);
	        			}
    					
    					
    					//modelbuilder.add("ta:Event_Annotation_"+uuid, RDF.TYPE, "oa:Annotation");
	        			//modelbuilder.add("ta:Event_Annotation_"+uuid, "oa:hasBody", "ta:EventBody_"+uuid);
	        			//modelbuilder.add("ta:Event_Annotation_"+uuid, "oa:hasTarget", "ta:EventTarget_"+uuid);
	        			
	        			//modelbuilder.add("ta:EventBody_"+uuid, RDF.TYPE, "ta:EventBody");
	        			//modelbuilder.add("ta:EventBody_"+uuid, "ta:topic", "ta:Topic_"+uuid);
	        			//modelbuilder.add("ta:EventBody_"+uuid, "ta:score", jarray.get(j).getAsJsonObject().get("score"));
	        			modelbuilder.add("ta:Event_Annotation_"+uuid, "ta:topic", "ta:Topic_"+uuid);
	        			modelbuilder.add("ta:Event_Annotation_"+uuid, "ta:score", jarray.get(j).getAsJsonObject().get("score"));
	        			
	        			
	        			modelbuilder.add("ta:Event_Annotation_"+uuid, "ta:rate", jarray.get(j).getAsJsonObject().get("change").getAsString());
	        			modelbuilder.add("ta:Event_Annotation_"+uuid, "ta:timestamp", jarray.get(j).getAsJsonObject().get("epoch_timestamp"));
	        			
	        			modelbuilder.add("ta:Topic_"+uuid, RDF.TYPE, "ta:Topic");
	        			modelbuilder.add("ta:Topic_"+uuid, RDFS.LABEL, jarray.get(j).getAsJsonObject().get("useCase").getAsString());
	        			//modelbuilder.add("ta:EventTarget_"+uuid, RDF.TYPE, "ta:EventTarget");
	        			//modelbuilder.add("ta:EventTarget_"+uuid, "ta:rate", jarray.get(j).getAsJsonObject().get("change"));
	        			//modelbuilder.add("ta:EventTarget_"+uuid, "ta:timestamp", jarray.get(j).getAsJsonObject().get("epoch_timestamp"));
	        			
    				}	    	
    			}
		    	Model model = modelbuilder.build();
		    	return model;
    		}		
    	return null;
    }
   public static Model buildEventsModel_final(String jsonLine) throws JsonSyntaxException, JsonIOException, IOException, InterruptedException {
		
		
		ModelBuilder modelbuilder = new ModelBuilder();
		// Write json input in a file and then read json file
		PrintWriter writer = new PrintWriter("json.json", "UTF-8");
    	writer.println(jsonLine);
    	writer.close();
    	Gson gson = new Gson();
    	JsonElement json = gson.fromJson(new FileReader("json.json"), JsonElement.class);
    	System.out.println("JSON"+json);
    	/*if(json.getAsJsonObject().has("events")) {
    		System.out.println("has");
    	}*/
    	JsonObject jobj = json.getAsJsonObject();
    	
    	ValueFactory factory = SimpleValueFactory.getInstance();
    	String newstring, location, eventtype;
		//if(jarray.size()!=0) {
    	if(jobj.has("usecase") && jobj.has("score")  && jobj.has("change") && jobj.has("timestamp") && jobj.has("language") && jobj.has("keywords")) {
			
    			modelbuilder.setNamespace("as", Namespaces.AS).setNamespace("oa", Namespaces.OA).setNamespace("owl",Namespaces.OWL).setNamespace("rdf", Namespaces.RDF).
				setNamespace("rdfs", Namespaces.RDFS).setNamespace("ta", Namespaces.TA).setNamespace("tweets", Namespaces.TWEETS).setNamespace("xsd", Namespaces.XSD)
				.setNamespace("wgs84_pos", Namespaces.WGS84).setNamespace("ogc",Namespaces.OGC);
    			
    		
    			String[] data = new String [2];
    			//for(int j=0; j<jarray.size(); j++) {
    					String uuid = UUID.randomUUID().toString().replaceAll("-", "");
	    				
	        			modelbuilder.add("ta:Event_Annotation_"+uuid, RDF.TYPE, "ta:Event"+jobj.get("usecase").getAsString());
	        			modelbuilder.add("ta:Event_Annotation_"+uuid, "oa:hasBody", "ta:Topic_"+uuid);
	        			modelbuilder.add("ta:Event_Annotation_"+uuid, "oa:hasTarget", "ta:Event_Target_"+uuid);
	        			
	        			modelbuilder.add("ta:Event_Target_"+uuid, RDF.TYPE, "ta:EventTarget");
	        			modelbuilder.add("ta:Event_Target_"+uuid, "ta:language", jobj.get("language").getAsString());
	        	
	        			
	        			modelbuilder.add("ta:Event_Target_"+uuid, "ta:score", jobj.get("score").getAsString());
	        			
	        			
	        			modelbuilder.add("ta:Event_Target_"+uuid, "ta:rate", jobj.get("change").getAsString());
	        			modelbuilder.add("ta:Event_Target_"+uuid, "ta:timestamp", jobj.get("timestamp"));
	        			
	        			modelbuilder.add("ta:Topic_"+uuid, RDF.TYPE, "ta:Topic");
	        			modelbuilder.add("ta:Topic_"+uuid, RDFS.LABEL, jobj.get("usecase").getAsString());
	        			for(int i=0; i<jobj.get("keywords").getAsJsonArray().size(); i++) {
	        				//modelbuilder.add("ta:Topic_"+uuid, "ta:hasKeywords", jobj.get("keywords").getAsJsonArray().get(i).getAsString());
	        				modelbuilder.add("ta:Topic_"+uuid, "ta:hasKeywords","ta:Label_"+uuid+i);
	        				JSONObject babel_res = new JSONObject ();
		        			babel_res=calculateBabel(jobj.get("keywords").getAsJsonArray().get(i).getAsString());
		        			modelbuilder.add("ta:Label_"+uuid+i, RDF.TYPE, "ta:Label");
		        			modelbuilder.add("ta:Label_"+uuid+i, RDFS.LABEL, jobj.get("keywords").getAsJsonArray().get(i).getAsString());
		        			modelbuilder.add("ta:Label_"+uuid+i, "ta:hasBabel", "ta:BabelEntity_"+uuid+i);
		        			
		        			modelbuilder.add("ta:BabelEntity_"+uuid+i, RDF.TYPE, "ta:BabelEntity");
		        			if(babel_res.containsKey("Level 1")) {
		        				modelbuilder.add("ta:BabelEntity_"+uuid+i, "ta:level1", babel_res.get("Level 1"));
		        			}
		        			else {
		        				modelbuilder.add("ta:BabelEntity_"+uuid+i, "ta:level1", "null");
		        			}
		        			if(babel_res.containsKey("Level 2")) {
		        			modelbuilder.add("ta:BabelEntity_"+uuid+i, "ta:level2", babel_res.get("Level 2"));
		        			}
		        			else {
		        				modelbuilder.add("ta:BabelEntity_"+uuid+i, "ta:level2", "null");
		        			}
		        			if(babel_res.containsKey("Level 3")) {
		        			modelbuilder.add("ta:BabelEntity_"+uuid+i, "ta:level3", babel_res.get("Level 3"));
		        			}
		        			else {
		        				modelbuilder.add("ta:BabelEntity_"+uuid+i, "ta:level3", "null");
		        			}
	        			}
	        			
	        			if(jobj.has("location") && jobj.has("point")) {

	        				modelbuilder.add("ta:Topic_"+uuid, "ta:location", "ta:Location_"+uuid);
	        				modelbuilder.add("ta:Location_"+uuid, RDF.TYPE, "ta:Location");
	        				
	        				modelbuilder.add("ta:Location_"+uuid, RDFS.LABEL, jobj.get("location").getAsString());
	        				if(jobj.get("point").getAsJsonObject().has("x") && jobj.get("point").getAsJsonObject().has("y")) {
	        					//modelbuilder.add("ta:Location_"+uuid, "wgs84_pos:lat", Float.parseFloat(jobj.get("point").getAsJsonObject().get("x").getAsString()));		
	        					//modelbuilder.add("ta:Location_"+uuid, "wgs84_pos:long", Float.parseFloat(jobj.get("point").getAsJsonObject().get("y").getAsString()));
	        					modelbuilder.add("ta:Location_"+uuid, "ta:hasPoint", "ta:Point_"+uuid);
			    				modelbuilder.add("ta:Point_"+uuid, RDF.TYPE, "ogc:Geometry");
			    				String point="<http://www.opengis.net/def/crs/OGC/1.3/CRS84> POINT(";
			    				point=point+ jobj.get("point").getAsJsonObject().get("x").getAsString()+" "+ jobj.get("point").getAsJsonObject().get("y").getAsString();
			    				point=point+")^^ogc:wktLiteral";
			    				modelbuilder.add("ta:Point_"+uuid, "ogc:asWKT", point);
			        			//modelbuilder.add("ta:ClusterBody_"+jarray.get(j).getAsJsonObject().get("id").getAsString(), "wgs84_pos:lat", Float.parseFloat(jarray.get(j).getAsJsonObject().get("location").getAsJsonObject().get("latitude").getAsString()));		
			        			//modelbuilder.add("ta:ClusterBody_"+jarray.get(j).getAsJsonObject().get("id").getAsString(), "wgs84_pos:long", Float.parseFloat(jarray.get(j).getAsJsonObject().get("location").getAsJsonObject().get("longitude").getAsString()));
			    			
	        				}
		        			
		    			}
    					    	
    			
		    	Model model = modelbuilder.build();
		    	return model;
    		}		
    	return null;
    }
	public static Model buildChangeDetectionModel(String jsonLine) throws JsonSyntaxException, JsonIOException, IOException {
		
		boolean istop;
			String uuid = UUID.randomUUID().toString().replaceAll("-", "");
			
			ModelBuilder modelbuilder = new ModelBuilder();
			// Write json input in a file and then read json file
			PrintWriter writer = new PrintWriter("json.json", "UTF-8");
	    	writer.println(jsonLine);
	    	writer.close();
	    	Gson gson = new Gson();
	    	JsonElement json = gson.fromJson(new FileReader("json.json"), JsonElement.class);
	    	JsonObject jobject = json.getAsJsonObject().get("flood_map").getAsJsonObject();
	    	ValueFactory factory = SimpleValueFactory.getInstance();
			
			if(jobject.has("sensing_date")) {
	    	   	
	    			modelbuilder.setNamespace("as", Namespaces.AS).setNamespace("oa", Namespaces.OA).setNamespace("owl",Namespaces.OWL).setNamespace("rdf", Namespaces.RDF).
					setNamespace("rdfs", Namespaces.RDFS).setNamespace("ta", Namespaces.TA).setNamespace("tweets", Namespaces.TWEETS).setNamespace("xsd", Namespaces.XSD)
					.setNamespace("wgs84_pos", Namespaces.WGS84).setNamespace("ogc", Namespaces.OGC);
	    			
	    			
	    			//for(int j=0; j<jarray.size(); j++) {
	    			//if(jarray.get(j).getAsJsonObject().has("id") && jarray.get(j).getAsJsonObject().has("labels")  && jarray.get(j).getAsJsonObject().has("tweets") && jarray.get(j).getAsJsonObject().has("top_ranked_tweets")) {
	    			 	
		    		modelbuilder.add("ta:ChangeDetectionAnnotation_"+uuid, RDF.TYPE, "oa:Annotation");
		        	modelbuilder.add("ta:ChangeDetectionAnnotation_"+uuid, "oa:hasBody", "ta:ChangeDetectionBody_"+uuid);
		        	modelbuilder.add("ta:ChangeDetectionAnnotation_"+uuid, "oa:hasTarget", "ta:ChangeDetectionTarget_"+uuid);
		        			
		        			
		        	modelbuilder.add("ta:ChangeDetectionBody_"+uuid, RDF.TYPE, "ta:ChangeDetectionBody");
		        	modelbuilder.add("ta:ChangeDetectionBody_"+uuid, "ta:hasFloodMapFile", jobject.get("flood_map").getAsString());
		        	modelbuilder.add("ta:ChangeDetectionBody_"+uuid, "ta:hasDate", jobject.get("sensing_date").getAsString());
		        	modelbuilder.add("ta:ChangeDetectionBody_"+uuid, "ta:comesFrom", jobject.get("satellite_constellation").getAsString());
		        	modelbuilder.add("ta:ChangeDetectionBody_"+uuid, "ta:isFlooded", jobject.get("is_flooded").getAsString());
		        			
		        	modelbuilder.add("ta:ChangeDetectionTarget_"+uuid, RDF.TYPE, "ta:ChangeDetectionTarget");
		        	modelbuilder.add("ta:ChangeDetectionTarget_"+uuid, RDFS.LABEL, jobject.get("area_geo_info").getAsJsonObject().get("properties").getAsJsonObject().get("name").getAsString());
		        	modelbuilder.add("ta:ChangeDetectionTarget_"+uuid, "ta:hasArea", jobject.get("area_geo_info").getAsJsonObject().get("properties").getAsJsonObject().get("area").getAsString());
		        	modelbuilder.add("ta:ChangeDetectionTarget_"+uuid, "ta:hasFloodedArea", jobject.get("flooded_sq_meters").getAsString());
		        	modelbuilder.add("ta:ChangeDetectionTarget_"+uuid, "ta:hasPercentage", jobject.get("flood_percent").getAsString());
		        	
		        	modelbuilder.add("ta:ChangeDetectionTarget_"+uuid, "ta:hasPolygon", "ta:FloodPolygon_"+uuid);
		        	modelbuilder.add("ta:FloodPolygon_"+uuid, RDF.TYPE, "ogc:Geometry");
		        	//<http://www.opengis.net/def/crs/EPSG/0/4326> MULTIPOLYGON ((
		        	// if ok or needs multipolygon
		        	//String polygon="<http://www.opengis.net/def/crs/OGC/1.3/CRS84> Polygon((";
		        	String polygon="<http://www.opengis.net/def/crs/EPSG/0/4326> MULTIPOLYGON (((";
		        	for (int i=0; i<jobject.get("area_geo_info").getAsJsonObject().get("geometry").getAsJsonObject().get("coordinates").getAsJsonArray().get(0).getAsJsonArray().size(); i++) {
		        		polygon+=jobject.get("area_geo_info").getAsJsonObject().get("geometry").getAsJsonObject().get("coordinates").getAsJsonArray().get(0).getAsJsonArray().get(i).getAsJsonArray().get(0).getAsString()+" ";
		        		if(i<jobject.get("area_geo_info").getAsJsonObject().get("geometry").getAsJsonObject().get("coordinates").getAsJsonArray().get(0).getAsJsonArray().size()-1) {
		        			polygon+=jobject.get("area_geo_info").getAsJsonObject().get("geometry").getAsJsonObject().get("coordinates").getAsJsonArray().get(0).getAsJsonArray().get(i).getAsJsonArray().get(1).getAsString()+", ";
		        		}
		        		else {
		        			polygon+=jobject.get("area_geo_info").getAsJsonObject().get("geometry").getAsJsonObject().get("coordinates").getAsJsonArray().get(0).getAsJsonArray().get(i).getAsJsonArray().get(1).getAsString()+"";
		        		}

		        		System.out.println("polygon:"+polygon);
		        	}
		        	polygon+=")))^^<http://www.opengis.net/ont/geosparql#wktLiteral>";
		        	System.out.println("polygon:"+polygon);
		        	modelbuilder.add("ta:FloodPolygon_"+uuid, "ogc:asWKT", polygon);
		        	
		        	
		        			
	    			
			    	Model model = modelbuilder.build();
			    	return model;
	    		}		
	    	return null;
	
	}
	public static JSONObject calculateBabel (String currentText) throws IOException, InterruptedException {
		
		
	    JSONObject terms = new JSONObject();
		ArrayList<String> idlist=new ArrayList<String>();
	    JSONObject obj;
	    String id;	
		//JsonElement json = gson.fromJson(currentText, JsonElement.class);
		//JsonArray labels = json.getAsJsonArray();
		JSONArray wordnet;
		JSONArray alljson = new JSONArray();
		
		
			
			//JsonArray labels = jobject.get(a).getAsJsonObject().get("labels").getAsJsonArray();
		//System.out.println("a"+labels.size());
			List<BabelfyInfo> results = new ArrayList<BabelfyInfo>();
			BabelfyText babel = new BabelfyText();
			
			obj = new JSONObject();
			
			
			wordnet = new JSONArray();
			
				System.out.println(currentText);
			
				results=babel.getSentence(currentText);
				
				for (int i=0;i<results.size();i++) {
					
					
					id=BabelfyText.read(results.get(i).babelURL);
					System.out.println("babelurl: "+results.get(i).babelURL);
					//if(!id.equals(null)) {
						for (int k=0;k<3; k++) {
							System.out.println(id);
							//id=ConnectToRepository.addId(id);
							
							id=RetrieveHypernym.retrieve(id);
							
							idlist.add(id);					
						}
					//}
					if (idlist.size()==0) {
						for(int j=0; j<3; j++) {
							idlist.add(null);
						}
					}
					
					terms = new JSONObject();
					terms.put("Term", results.get(i).frag);
					
					for(int j=0; j<idlist.size(); j++) {
						terms.put("Level "+(j+1), idlist.get(j));
					}
					
					//if(!wordnet.contains(terms)) {
						//wordnet.add(terms);
					//}
					idlist.clear();				
				}
						
				
				//obj.put("Wordnet", wordnet);
				//alljson.add(obj);		
			
			//FileWriter writer = new FileWriter("jsonresults_loc.json");
			//writer.write(alljson.toJSONString());
			//writer.close();
			System.out.println(alljson);
			return terms;
		
	}
	public static Model buildTweetModel(String jsonLine) throws JsonSyntaxException, JsonIOException, IOException {
		
		
		String uuid;// = UUID.randomUUID().toString().replaceAll("-", "");
	
		ModelBuilder modelbuilder = new ModelBuilder();
		// Write json input in a file and then read json file
		PrintWriter writer = new PrintWriter("tweet-json.json", "UTF-8");
    	writer.println(jsonLine);
    	writer.close();
    	Gson gson = new Gson();
    	
    	JsonElement json = gson.fromJson(new FileReader("tweet-json.json"), JsonElement.class);
    	//JsonArray jarray = json.getAsJsonObject().get("items").getAsJsonArray();
    	ValueFactory factory = SimpleValueFactory.getInstance();
    	System.out.println(json);
    	//System.out.println(jarray);
		//if(jarray.size()!=0) {
    	   	if(json.getAsJsonObject().has("id") && json.getAsJsonObject().has("timestamp") && json.getAsJsonObject().has("usecase") && json.getAsJsonObject().has("language") && json.getAsJsonObject().has("point") && json.getAsJsonObject().has("location")) {
    			//for(int j=0; j<jarray.size(); j++) {
    	   		modelbuilder.setNamespace("as", Namespaces.AS).setNamespace("oa", Namespaces.OA).setNamespace("owl",Namespaces.OWL).setNamespace("rdf", Namespaces.RDF).
				setNamespace("rdfs", Namespaces.RDFS).setNamespace("ta", Namespaces.TA).setNamespace("tweets", Namespaces.TWEETS).setNamespace("xsd", Namespaces.XSD)
				.setNamespace("wgs84_pos", Namespaces.WGS84).setNamespace("ogc", Namespaces.OGC).setNamespace("time", "https://www.w3.org/2006/time#");
    				uuid = UUID.randomUUID().toString().replaceAll("-", "");
    					modelbuilder.add("ta:Tweet_Annotation_"+uuid, RDF.TYPE, "oa:Annotation");
    					modelbuilder.add("ta:Tweet_Annotation_"+uuid, "oa:hasBody", "ta:TweetBody_"+uuid);
	        			modelbuilder.add("ta:Tweet_Annotation_"+uuid, "oa:hasTarget", "ta:TweetTarget_"+uuid);
	        			
	        			modelbuilder.add("ta:TweetTarget_"+uuid, RDF.TYPE, "ta:TweetTarget");
	        			
	        			modelbuilder.add("ta:TweetTarget_"+uuid, "ta:location", "ta:Location_"+uuid);
	        			modelbuilder.add("ta:Location_"+uuid, RDF.TYPE, "ta:Location");
	        				
	        			modelbuilder.add("ta:Location_"+uuid, RDFS.LABEL, json.getAsJsonObject().get("location").getAsString());
	        			if(json.getAsJsonObject().get("point").getAsJsonObject().has("x") && json.getAsJsonObject().get("point").getAsJsonObject().has("y")) { 
	        				
	        				
	        					modelbuilder.add("ta:Location_"+uuid, "ta:hasPoint", "ta:Point_"+uuid);
			    				modelbuilder.add("ta:Point_"+uuid, RDF.TYPE, "ogc:Geometry");
			    				String point="<http://www.opengis.net/def/crs/OGC/1.3/CRS84> POINT(";
			    				point=point+ json.getAsJsonObject().get("point").getAsJsonObject().get("x").getAsString()+" "+ json.getAsJsonObject().get("point").getAsJsonObject().get("y").getAsString();
			    				point=point+")^^ogc:wktLiteral";
			    				modelbuilder.add("ta:Point_"+uuid, "ogc:asWKT", point);
			        			
	        				
	        				
	        			}
	        			
	        			modelbuilder.add("ta:TweetTarget_"+uuid, "ta:hasId", json.getAsJsonObject().get("id").getAsString());
	        			
	        			
	        			modelbuilder.add("ta:TweetBody_"+uuid, RDF.TYPE, "ta:TweetBody");
	        			
	        			modelbuilder.add("ta:TweetBody_"+uuid, "time:inXSDDateTimeStamp", json.getAsJsonObject().get("timestamp").getAsString());
	        			modelbuilder.add("ta:TweetBody_"+uuid, "ta:hasUseCase", json.getAsJsonObject().get("usecase").getAsString());
	        			modelbuilder.add("ta:TweetBody_"+uuid, "ta:hasLanguage", json.getAsJsonObject().get("language").getAsString());
	        				
    				
		    	Model model = modelbuilder.build();
		    	
		    	return model;
    		}		
    	return null;
    }		
	
	/*public static String polygonQueryExecution(String polygon) {
		String query="PREFIX wgs84_pos: <http://www.w3.org/2003/01/geo/wgs84_pos#>\r\n" + 
				"PREFIX ogc: <http://www.opengis.net/ont/geosparql#>\r\n" + 
				"PREFIX geof: <http://www.opengis.net/def/function/geosparql/>\r\n" + 
				"PREFIX geo: <http://www.opengis.net/ont/geosparql#>\r\n" + 
				"PREFIX uom: <http://www.opengis.net/def/uom/OGC/1.0/>\r\n" + 
				"PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>\r\n" + 
				"PREFIX ta: <https://eopen-project.eu/ontologies/tweet-annotations#>\r\n" + 
				"PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\r\n" + 
				"PREFIX oa: <http://www.w3.org/ns/oa#>\r\n" + 
				"select ?event ?lang ?distance where {\r\n" + 
				"    ?r oa:hasBody ?b.\r\n" + 
				"    ?b rdfs:label ?event.\r\n" + 
				"    ?b ta:location ?a.\r\n" + 
				"    ?a ta:hasPoint ?s. \r\n" + 
				"    ?a rdfs:label ?lbl.\r\n" + 
				"    ?s ogc:asWKT ?o .\r\n" + 
				"    ?r oa:hasTarget ?t.\r\n" + 
				"    ?t ta:language ?lang.\r\n" + 
				" \r\n" + 
				"BIND(geof:distance(?o, '''<http://www.opengis.net/def/crs/OGC/1.3/CRS84>\r\n" + 
				"            Polygon (("+polygon+"))'''^^geo:wktLiteral, uom:metre) AS ?distance)\r\n" + 
				"    FILTER(?distance=\"0.0\"^^xsd:double)\r\n" + 
				"} \r\n" + 
				"\r\n" + 
				"";
		Repository twitterRepository = new HTTPRepository(Server.SERVER, Server.REPOSITORY);
		twitterRepository.initialize();
		RepositoryConnection twitterConnection = twitterRepository.getConnection();
        
System.out.println(query);
          
        TupleQuery query1 = twitterConnection.prepareTupleQuery(query);
        
        JSONObject event = new JSONObject ();
        JSONArray ev = new JSONArray ();
        JSONObject results = new JSONObject ();
        TupleQueryResult qresult1 = query1.evaluate();
        while (qresult1.hasNext()) {
            BindingSet st1 = qresult1.next();
            if(st1.hasBinding("event") && st1.hasBinding("lang") && st1.hasBinding("distance")) {
            	
            	event = new JSONObject ();
            	event.put("event", st1.getValue("event").stringValue());
            	event.put("language", st1.getValue("lang").stringValue());
            	ev.add(event);          	
            }
        }
        results.put("events", ev);
		//event
        query="PREFIX wgs84_pos: <http://www.w3.org/2003/01/geo/wgs84_pos#>\r\n" + 
				"PREFIX ogc: <http://www.opengis.net/ont/geosparql#>\r\n" + 
				"PREFIX geof: <http://www.opengis.net/def/function/geosparql/>\r\n" + 
				"PREFIX geo: <http://www.opengis.net/ont/geosparql#>\r\n" + 
				"PREFIX uom: <http://www.opengis.net/def/uom/OGC/1.0/>\r\n" + 
				"PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>\r\n" + 
				"PREFIX ta: <https://eopen-project.eu/ontologies/tweet-annotations#>\r\n" + 
				"PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\r\n" + 
				"PREFIX oa: <http://www.w3.org/ns/oa#>\r\n" + 
        		"select ?usecase ?lang ?ts ?distance where {\r\n" + 
        		"    ?r oa:hasBody ?b.\r\n" + 
        		"    ?r oa:hasTarget ?target.\r\n" + 
        		"    ?target ta:location ?a.\r\n" + 
        		"    ?a ta:hasPoint ?s. \r\n" + 
        		"    ?a rdfs:label ?lbl.\r\n" + 
        		"    ?s ogc:asWKT ?o .\r\n" + 
        		"    ?b ta:hasUseCase ?usecase.\r\n" + 
        		"    ?b ta:hasLanguage ?lang.\r\n" + 
        		"    ?b time:inXSDDateTimeStamp ?ts." + 
        		"BIND(geof:distance(?o, '''<http://www.opengis.net/def/crs/OGC/1.3/CRS84>\r\n" + 
				"            Polygon (("+polygon+"))'''^^geo:wktLiteral, uom:metre) AS ?distance)\r\n" + 
				"    FILTER(?distance=\"0.0\"^^xsd:double)\r\n" + 
				"} \r\n" + 
				"\r\n" + 
				"";
        System.out.println(query);
        
		  query1 = twitterConnection.prepareTupleQuery(query);
	      
	       
	        JSONArray tw = new JSONArray ();
	        JSONObject tweet = new JSONObject ();
	        qresult1 = query1.evaluate();
	        while (qresult1.hasNext()) {
	            BindingSet st1 = qresult1.next();
	            if(st1.hasBinding("usecase") && st1.hasBinding("lang") && st1.hasBinding("distance")) {
	            	
	            	tweet = new JSONObject ();
	            	tweet.put("usecase", st1.getValue("usecase").stringValue());
	            	tweet.put("language", st1.getValue("lang").stringValue());
	            	tw.add(tweet);     	
	            }
	        }
	        results.put("has_tweets", tw);
        
		 query="PREFIX wgs84_pos: <http://www.w3.org/2003/01/geo/wgs84_pos#>\r\n" + 
				"PREFIX ogc: <http://www.opengis.net/ont/geosparql#>\r\n" + 
				"PREFIX geof: <http://www.opengis.net/def/function/geosparql/>\r\n" + 
				"PREFIX geo: <http://www.opengis.net/ont/geosparql#>\r\n" + 
				"PREFIX uom: <http://www.opengis.net/def/uom/OGC/1.0/>\r\n" + 
				"PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>\r\n" + 
				"PREFIX ta: <https://eopen-project.eu/ontologies/tweet-annotations#>\r\n" + 
				"PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\r\n" + 
				"PREFIX oa: <http://www.w3.org/ns/oa#>\r\n" + 
				"select ?distance ?area where {\r\n" + 
				"    ?a ?b ?s.\r\n" + 
				"    ?a ta:hasArea ?area.\r\n" + 
				"    ?s ogc:asWKT ?o .\r\n" + 
				"   \r\n" + 
				" \r\n" + 
				"BIND(geof:distance(?o, '''<http://www.opengis.net/def/crs/OGC/1.3/CRS84>\r\n" + 
				"            Polygon (("+polygon+"))'''^^geo:wktLiteral, uom:metre) AS ?distance)\r\n" + 
				"    FILTER(?distance=\"0.0\"^^xsd:double)\r\n" + 
				"} \r\n" + 
				"\r\n" + 
				"";
		//flood area
		  query1 = twitterConnection.prepareTupleQuery(query);
		  System.out.println(query);
	       
	        JSONArray fl = new JSONArray ();
	       
	        qresult1 = query1.evaluate();
	        while (qresult1.hasNext()) {
	            BindingSet st1 = qresult1.next();
	            if(st1.hasBinding("distance") && st1.hasBinding("area")) {
	            	
	            	fl.add(st1.getValue("area").stringValue());          	
	            }
	        }
	        results.put("has_flooded_areas", fl);
	        return results.toString();
	}*/
	static String splitCamelCase(String s) {
		   return s.replaceAll(
		      String.format("%s|%s|%s",
		         "(?<=[A-Z])(?=[A-Z][a-z])",
		         "(?<=[^A-Z])(?=[A-Z])",
		         "(?<=[A-Za-z])(?=[^A-Za-z])"
		      ),
		      " "
		   );
		}

}