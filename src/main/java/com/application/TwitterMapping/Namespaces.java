package com.application.TwitterMapping;


public class Namespaces {
	public static String AS = "http://www.w3.org/ns/activitystreams#";	
	public static String OA = "http://www.w3.org/ns/oa#";
	public static String OWL = "http://www.w3.org/2002/07/owl#";
	public static String RDF = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
	public static String RDFS = "http://www.w3.org/2000/01/rdf-schema#";
	public static String TA = "https://eopen-project.eu/ontologies/tweet-annotations#";
	public static String TWEETS = "https://eopen-project.eu/ontologies/tweets#";
	public static String XSD = "http://www.w3.org/2001/XMLSchema#";
	public static String WGS84 = "http://www.w3.org/2003/01/geo/wgs84_pos#";
	
	public static String OGC="http://www.opengis.net/ont/geosparql#";

}