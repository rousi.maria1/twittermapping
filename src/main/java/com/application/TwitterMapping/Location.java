package com.application.TwitterMapping;

public class Location {

	public String name; 
	public String latitude; 
	public String longitude;
	public Location(String n, String d, String e) {
		name=n;
		latitude=d;
		longitude=e;
	}
}
