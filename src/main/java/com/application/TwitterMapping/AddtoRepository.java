package com.application.TwitterMapping;

import java.io.FileInputStream;
import java.io.IOException;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryException;
import org.eclipse.rdf4j.repository.http.HTTPRepository;
import org.eclipse.rdf4j.repository.manager.RemoteRepositoryManager;
import org.eclipse.rdf4j.repository.manager.RepositoryManager;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFParseException;


public class AddtoRepository {

	public static void add(String name, String server) throws RepositoryException, RDFParseException, IOException {
			
		RepositoryManager repositoryManager = new RemoteRepositoryManager(server);
		repositoryManager.initialize();
		
		Repository twitterRepository = new HTTPRepository(server, Server.REPOSITORY);
		twitterRepository.initialize();
		RepositoryConnection twitterConnection = twitterRepository.getConnection();
		
		loadData(twitterConnection, name);
				
		twitterConnection.close();

	}
			
	public static void loadData(RepositoryConnection connection, String name) throws RepositoryException, IOException, RDFParseException {
			       
		connection.begin();	
		// Load files in GraphDB repository	
		connection.add(new FileInputStream(name), "urn:base", RDFFormat.TURTLE);	       
		// Committing the transaction persists the data
		connection.commit();		   
	}
	
	public static void clearData(RepositoryConnection connection) {
		
		// Clear the repository 
        connection.clear();
        connection.remove((Resource) null, null, null);       
	}
}
