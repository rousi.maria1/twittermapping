package com.application.TwitterMapping;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import it.uniroma1.lcl.babelfy.commons.BabelfyConstraints;
import it.uniroma1.lcl.babelfy.commons.BabelfyParameters;
import it.uniroma1.lcl.babelfy.commons.BabelfyParameters.MCS;
import it.uniroma1.lcl.babelfy.commons.BabelfyParameters.ScoredCandidates;
import it.uniroma1.lcl.babelfy.commons.BabelfyParameters.SemanticAnnotationResource;
import it.uniroma1.lcl.babelfy.commons.annotation.SemanticAnnotation;
import it.uniroma1.lcl.babelfy.core.Babelfy;
import it.uniroma1.lcl.jlt.util.Language;

public class BabelfyText {

	
	// Get Babelfy information about a sentence
	public List<BabelfyInfo> getSentence(String inputText) throws IOException, InterruptedException {
			
			BabelfyConstraints constraints = new BabelfyConstraints();
			//SemanticAnnotation a = new SemanticAnnotation(new TokenOffsetFragment(0, 0), "bn:03083790n", "http://dbpedia.org/resource/BabelNet", Source.OTHER);
			//constraints.addAnnotatedFragments(a);
			BabelfyParameters bp = new BabelfyParameters();
			bp.setAnnotationResource(SemanticAnnotationResource.BN);
			bp.setMCS(MCS.ON_WITH_STOPWORDS);
			bp.setScoredCandidates(ScoredCandidates.ALL);
			Babelfy bfy = new Babelfy(bp);
			//Thread.sleep(1);
			List<SemanticAnnotation> bfyAnnotations = bfy.babelfy(inputText, Language.EN, constraints);
			//List<SemanticAnnotation> bfyAnnotations = bfy.babelfy(inputText, Language.EL, constraints);
			
			
			List<BabelfyInfo> results = new ArrayList<BabelfyInfo>();
			Map<String, Double> map = new HashMap<String, Double>();
			// bfyAnnotations is the result of Babelfy.babelfy() call
			for (SemanticAnnotation annotation : bfyAnnotations)
			{
				
			    String frag = inputText.substring(annotation.getCharOffsetFragment().getStart(), annotation.getCharOffsetFragment().getEnd() + 1);
			    
				if(annotation.getDBpediaURL()!=null) {
					results.add(new BabelfyInfo(frag, annotation.getBabelSynsetID().toString(), annotation.getBabelNetURL().toString(), annotation.getSource().toString(), annotation.getCoherenceScore(), annotation.getDBpediaURL().toString(),annotation.getGlobalScore(), annotation.getScore()));
				    
				    // if frag does not exist in map add it
				    if(!map.containsKey(frag)) {		
				    	map.put(frag, annotation.getGlobalScore());
				    }
				    // else if frag exists keep the one with the best score
				    else if(map.containsKey(frag)){
				    	double value = map.get(frag);
				    	if(value<annotation.getGlobalScore()) {
				    		map.put(frag, annotation.getGlobalScore());
				    	}
				    }
				    System.out.println(annotation.getBabelNetURL());
				}
				else {
				    results.add(new BabelfyInfo(frag, annotation.getBabelSynsetID().toString(), annotation.getBabelNetURL().toString(), annotation.getSource().toString(), annotation.getCoherenceScore(), null, annotation.getGlobalScore(), annotation.getScore()));
				   
				    // if frag does not exist in map add it
				    if(!map.containsKey(frag)) {	
				    	map.put(frag, annotation.getGlobalScore());
				    }
				    // if frag exists keep the one with the best score
				    else if(map.containsKey(frag)){
				    	double value = map.get(frag);
				    	if(value<annotation.getGlobalScore()) {
				    		map.put(frag, annotation.getGlobalScore());
				    	}
				    }
				}
			}
			
			
			
			// keep the synsets with the best scores, using the map
		//was commented
			List<BabelfyInfo> cleaned = new ArrayList<BabelfyInfo>();
	
				Set<Map.Entry<String, Double>> set = map.entrySet();
			for(int i=0; i<results.size(); i++) {
				
				for (Map.Entry<String, Double> me : set) {
					System.out.println("---------------------");
					//System.out.println(cleaned.size());
					System.out.println(me.getKey() +" "+me.getValue());
					//System.out.println(results.get(i).frag.equals(me.getKey()) +" "+ results.get(i).a==me.getValue() && !cleaned.get(cleaned.size()-1).frag.equals(me.getKey()));
					if(i>0 && cleaned.size()>0) {
						if(results.get(i).frag.equals(me.getKey()) && results.get(i).a==me.getValue() && !cleaned.get(cleaned.size()-1).frag.equals(me.getKey())) {
							cleaned.add(results.get(i));    	 
					    }
				    }
				    else {
				    	//System.out.println(results.get(i).frag +" : "+me.getKey());
					    if(results.get(i).frag.equals(me.getKey()) && results.get(i).a==me.getValue()) {
					    	cleaned.add(results.get(i));	 
					    }
				    }
				}
			}
			System.out.println("---------------------");
			for(int i=0; i<cleaned.size(); i++ ) {
				System.out.println(cleaned.get(i).frag+ " : "+cleaned.get(i).a);
			}
			return cleaned;
			
		//was not commentedreturn results;	
		
	}
	
	// Find the lemon-WordNet31 id for each sense using babelnet linked data interface
	public static String read(String u) {
			URL url;
		    InputStream is = null;
		    BufferedReader br;
		    String line, result="";
		    System.out.println("BabelURL:"+u);
		   
		    try {
		        url = new URL(u);
		        HttpURLConnection conn =(HttpURLConnection) url.openConnection();
		        int status = conn.getResponseCode();
		       // System.out.println(status);
		        
		        if(status!=500) {
			        is = url.openStream(); 
			        br = new BufferedReader(new InputStreamReader(is));
			        
			        while ((line = br.readLine()) != null) {
			        	//System.out.println("!!!!!!!!!!!!"+line);
			        	if(line.contains("skos:exactMatch") && line.contains("lemon-WordNet31:")) {
			        		
			        		result=line.substring(line.indexOf("lemon-WordNet31:"));
				            char character=result.charAt(28);
				            result=StringUtils.substringBetween(line, "lemon-WordNet31:", " "+character);
			        		result=result.substring(1);
			        		System.out.println("Babel synset:"+result);
			        		
			        		return result;
			        	} 	
			        }
		        }
		    } catch (MalformedURLException mue) {
		         mue.printStackTrace();
		    } catch (IOException ioe) {
		         ioe.printStackTrace();
		    } finally {
		        try {
		            if (is != null) is.close();
		        } catch (IOException ioe) {
		            
		        }
		    }
		    return null;
		}
	
	
	// Find the lemon-WordNet31 id for each sense using babelnet linked data interface
		public static String getWordNetID(String u) {
			URL url;
		    InputStream is = null;
		    BufferedReader br;
		    String line, result="";
		    String newurl;
		    try {
		        url = new URL("http://160.40.51.145:8084/babelnet-wrapper/api/rest/get?babelnet="+u);
		        HttpURLConnection conn =(HttpURLConnection) url.openConnection();
		        int status = conn.getResponseCode();
		        //System.out.println(status);
		        if(status!=500) {
		        is = url.openStream(); 
		        br = new BufferedReader(new InputStreamReader(is));

		        while ((line = br.readLine()) != null) {
		        	
		        	
		        	//System.out.println(line);
		        	if (line.contains(",")) {
		        		int iend = line.indexOf(",");
		        		line=line.substring(0, iend);
		        	}
		        		//return line;
		        	 //System.out.println(findNewWordNetID(line)); 	
		        	 return findNewWordNetID(line);
		        }
		        }
		       
		    } catch (MalformedURLException mue) {
		         mue.printStackTrace();
		    } catch (IOException ioe) {
		         ioe.printStackTrace();
		    } finally {
		        try {
		            if (is != null) is.close();
		        } catch (IOException ioe) {
		            
		        }
		    }
		    return null;
		}
	
	// get new wordnet id using wordnet 3.0 id
	public static String findNewWordNetID(String old) throws IOException {
	
		String newid="";
		BufferedReader br = null;
		
        try {

        	String furl=old.replaceAll("wn:", "");
			String substr = furl.substring(furl.length() - 1);
			
			furl=furl.replaceAll(substr,"-"+substr+".json");		
			furl="http://wordnet-rdf.princeton.edu/pwn30/"+furl;
            
			URL url = new URL(furl);
            br = new BufferedReader(new InputStreamReader(url.openStream()));
          
            String line;
            StringBuilder sb = new StringBuilder();

            while ((line = br.readLine()) != null) {

                sb.append(line);
                sb.append(System.lineSeparator());
            }

            if(sb.toString().contains("\"id\":\"")) {
            	newid=sb.toString().substring(sb.toString().indexOf("\"id\":\""));
            	newid=newid.substring(6, 16);
            	//System.out.println(newid);
            }
            if(newid=="") {
            	furl=old.replaceAll("wn:", "");
    			substr = furl.substring(furl.length() - 1);
    			if(substr.equals("a")) {
    				
    				furl=furl.replaceAll(substr,"-"+"s"+".json");		
    			}
    			furl="http://wordnet-rdf.princeton.edu/pwn30/"+furl;
    			//System.out.println(furl);
                url = new URL(furl);
                
                br = new BufferedReader(new InputStreamReader(url.openStream()));
                sb = new StringBuilder();

                while ((line = br.readLine()) != null) {
                    sb.append(line);
                    sb.append(System.lineSeparator());
                }

                if(sb.toString().contains("\"id\":\"")) {
                	newid=sb.toString().substring(sb.toString().indexOf("\"id\":\""));
                	newid=newid.substring(6, 16); 	
                }
            }   
        } finally {

            if (br != null) {
                br.close();
            }
        }
        return newid;
	
	}
}