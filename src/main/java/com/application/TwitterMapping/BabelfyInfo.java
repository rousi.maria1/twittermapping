package com.application.TwitterMapping;

public class BabelfyInfo {

	public String frag;
	public String synset;
	public String babelURL;
	public String dbURL;
	public String source;
	public double score;
	public String url;
	public double a, b;
	
	public BabelfyInfo(String fr, String syn, String burl, String src, double sc, String u, double c, double d) {
		frag=fr;
		synset=syn;
		babelURL=burl;
		source=src;
		score=sc;
		url=u;
		a=c;
		b=d;
	}
	
}
