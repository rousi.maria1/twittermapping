package com.application.TwitterMapping;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;


import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

/** Example resource class hosted at the URI path "/myresource"
 */
//mapping/
@Path("/{task}")
public class MyResource {
    
    /** Method processing HTTP GET requests, producing "text/plain" MIME media
     * type.
     * @return String that will be send back as a response of type "text/plain".
     * @throws IOException 
     * @throws JsonIOException 
     * @throws JsonSyntaxException 
     * @throws InterruptedException 
     */
	

	    @POST
	   // @Path("/mapping")
	    @Consumes(MediaType.APPLICATION_JSON)
	    public Response createDataInJSON(@PathParam("task") String task, String data) throws JsonSyntaxException, JsonIOException, IOException, InterruptedException { 
	    	 String uuid = UUID.randomUUID().toString().replaceAll("-", "");

	    String javaHome = System.getenv("GRAPHDB_ADDRESS");	 
		String currentLine, allfile="";
		String text=data;
		System.out.println(text);
		Model selectedmodel=null;
		// Check if input is empty
		System.out.println(text.isEmpty());
		System.out.println(isJSONValid(text));
		if(!text.isEmpty()) {
			//if(text.contains("flood_map")) { 
			if(task.equals("flood_map")) {
				// Get selected model and create its model			
					selectedmodel = Mapping.buildChangeDetectionModel(text);	
			}
			//else if(text.contains("keywords")) {
			else if(task.equals("events")) {
			// Get selected model and create its model			
				selectedmodel = Mapping.buildEventsModel_final(text);	
			}
			 //else if(text.contains("labels")) {
			else if(task.equals("topics")) {
			// Get selected model and create its model			
				selectedmodel = Mapping.buildModel_final(text);	
			}
			 //else if(text.contains("id")) {
			else if(task.equals("tweet")) {
					// Get selected model and create its model			
						selectedmodel = Mapping.buildTweetModel(text);	
					}
			 
			
			// Print the selected model if exists, else print "The model is null!"
			if(selectedmodel!=null) {	
				// Write results in json type in file
				OutputStream out = new FileOutputStream("twitter"+uuid+".ttl");
				Rio.write(selectedmodel, out, RDFFormat.TURTLE);
				out.close();
				    	
				// Read results from file
				File file = new File("twitter"+uuid+".ttl");
				BufferedReader br = new BufferedReader(new FileReader(file));
				while ((currentLine = br.readLine()) != null) {	
					allfile=allfile+currentLine;
					System.out.println(currentLine);
				}
				br.close();
				System.out.println("twitter"+uuid+".ttl");
				
				AddtoRepository.add("twitter"+uuid+".ttl", javaHome);
				Files.deleteIfExists(Paths.get("twitter"+uuid+".ttl")); 
				return Response.status(201).entity(allfile).build();
				//return Response.status(201).entity("Data successfully saved in the KB!").build();
			}
		}	
		
		return Response.status(201).entity("The model is null!").build();
	}
			

	// Check if input is JSON valid
	public boolean isJSONValid(String test) {
	    try {
	    	new JSONArray(test);
	    } catch (JSONException ex) {
	       return false;		        
	    }
		return true;
	}
}
