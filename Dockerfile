FROM java:8

# Install maven
RUN apt-get -y update && apt-get install -y maven

WORKDIR /code

# Prepare by downloading dependencies
ADD pom.xml /code/pom.xml
ADD server.xml /code/server.xml

# Prepare by downloading dependencies
ADD /libs/babelfy-commons-1.0.jar /code/babelfy-commons-1.0.jar
ADD /libs/babelfy-online-1.0.jar /code/babelfy-online-1.0.jar
ADD /libs/jltutils-2.0.4.jar /code/jltutils-2.0.4.jar
ADD /libs/commons-configuration-1.5.jar /code/commons-configuration-1.5.jar
ADD /libs/commons-collections-3.2.jar /code/commons-collections-3.2.jar
ADD /libs/commons-lang-2.3.jar /code/commons-lang-2.3.jar
ADD /libs/commons-logging-1.1.3.jar /code/commons-logging-1.1.3.jar
ADD /libs/gson-2.3.1.jar /code/gson-2.3.1.jar
ADD /libs/httpclient-4.3.6.jar /code/httpclient-4.3.6.jar
ADD /libs/httpcore-4.3.3.jar /code/httpcore-4.3.3.jar
ADD /libs/httpmime-4.3.6.jar /code/httpmime-4.3.6.jar

ENV GRAPHDB_ADDRESS default_env_value

ADD babelfy.properties /code/config/babelfy.properties
ADD babelfy.var.properties /code/config/babelfy.var.properties

# Adding source, compile and package into a fat jar
ADD src /code/src
RUN ["mvn", "package"]

EXPOSE 7203
CMD ["mvn", "tomcat7:run"]